INTRODUCTION
------------

This module is designed to move all help text above the fields, and lightly restyle.

REQUIREMENTS
------------

None.

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. See:
  https://www.drupal.org/docs/8/extending-drupal-8/installing-modules
  for further information.

You may also install this via composer:

```
composer require idfive/idfive_move_description
```

CONFIGURATION
-------------

None.


TROUBLESHOOTING
---------------

Enable print_r() or dsm() in idfive_move_description_preprocess_form_element().
